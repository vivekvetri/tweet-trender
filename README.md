Overview
=========

This is an open-source project for finding the trending topics out of a realtime twitter stream using a time-sensitive rolling count algorithm. It uses twitter4j for connecting with the twitter stream and making it as trending topics. Feel free to clone the project for learning/development purposes. A dia file have been added to get an overall idea about the storm topology being discussed. This project is highly prone to development.

Trending topics
================

*A word, phrase or topic that is tagged at a greater rate than other tags is said to be a trending topic. Trending topics become popular either through a concerted effort by users or because of an event that prompts people to talk about one specific topic. These topics help Twitter and their user to understand what is happening in the world*

Project Setup
=============

To use this tool you need a valid twitter account and you have to specify your AccessToken, AccessTokenSecret, ConsumerKey, ConsumerSecret as environment variables.

Using Eclipse IDE
==================

1.  Create a project and link this project's src directory to it.
2.  Add all the necessary jar files in the lib/ folder.
3.  Right-click the project name.
4.  Select "Properties"->"Run/Debug settings"->"New" configuration->"Environment" tab. 
5.  Add the following environment variables with the values you got from dev.twitter.com 

```
twitter4j.debug=true
twitter4j.oauth.consumerKey=*********************
twitter4j.oauth.consumerSecret=******************************************
twitter4j.oauth.accessToken=**************************************************
twitter4j.oauth.accessTokenSecret=******************************************
```

Refer [here](http://twitter4j.org/en/configuration.html) for more details on this.

If you have any doubts, feel free to mail me.

MailID: *VivekVetri@iCloud.com*