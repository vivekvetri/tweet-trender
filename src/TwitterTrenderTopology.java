/* Author : VivekVetri <vivekvetri@icloud.com> */

/*This file is part of Tweet-Trender.

    Tweet-Trender is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tweet-Trender is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tweet-Trender.  If not, see <http://www.gnu.org/licenses/>.
 */   	

import bolt.SplitTweet;
import bolt.IntermediateRankingsBolt;
import bolt.RollingCountBolt;
import bolt.TotalRankingsBolt;
import spout.TwitterSpout;
import utils.StormRunner;
import backtype.storm.Config;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;

/**
 * This topology does a continuous computation of the top N words that the topology has seen in terms of cardinality.
 * The top N computation is done in a completely scalable way, and a similar approach could be used to compute things
 * like trending topics or trending images on Twitter.
 */
public class TwitterTrenderTopology {
	  private static final int DEFAULT_RUNTIME_IN_SECONDS = 60;
	  private static final int TOP_N = 5;

	  private final TopologyBuilder builder;
	  private final String topologyName;
	  private final Config topologyConfig;
	  private final int runtimeInSeconds;

	  public TwitterTrenderTopology() throws InterruptedException {
	    builder = new TopologyBuilder();
	    topologyName = "slidingWindowCounts";
	    topologyConfig = createTopologyConfiguration();
	    runtimeInSeconds = DEFAULT_RUNTIME_IN_SECONDS;

	    wireTopology();
	  }

	  private static Config createTopologyConfiguration() {
	    Config conf = new Config();
	    conf.setDebug(true);
	    return conf;
	  }

	  private void wireTopology() throws InterruptedException {
		    //use TwitterSpout
			String twitterSpoutId = "tweets";
			int twitterSpoutParallelism = 1;
			builder.setSpout(twitterSpoutId, new TwitterSpout(), twitterSpoutParallelism);
			
			//use SplitTweet Bolt
			String splitTweetBoltId = "split_tweet";
			int splitTweetParallelism = 5;
			builder.setBolt(splitTweetBoltId, new SplitTweet(), splitTweetParallelism)
					.fieldsGrouping(twitterSpoutId, new Fields("status"));
						
			//use RollingCount Bolt
			String counterId = "count_word";
			builder.setBolt(counterId, new RollingCountBolt(9, 3), 4).fieldsGrouping(splitTweetBoltId, new Fields("word"));
			
			//use IntermediateRanker Bolts
			String intermediateRankerId = "intermediateRanker";
			builder.setBolt(intermediateRankerId, new IntermediateRankingsBolt(TOP_N), 4).fieldsGrouping(counterId, new Fields("obj"));
			
			//use FinalRanker
			String totalRankerId = "finalRanker";
			builder.setBolt(totalRankerId, new TotalRankingsBolt(TOP_N)).globalGrouping(intermediateRankerId);		

	  }

	  public void run() throws InterruptedException {
	    StormRunner.runTopologyLocally(builder.createTopology(), topologyName, topologyConfig, runtimeInSeconds);
	  }

	  public static void main(String[] args) throws Exception {
	    new TwitterTrenderTopology().run();
	  }
	  
}
