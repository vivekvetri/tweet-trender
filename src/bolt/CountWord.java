/* Author : VivekVetri <vivekvetri@icloud.com> */

/*This file is part of Tweet-Trender.

    Tweet-Trender is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tweet-Trender is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tweet-Trender.  If not, see <http://www.gnu.org/licenses/>.
 */   	

package bolt;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.mutable.MutableInt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;


/**
 *  Normal Count Word bolt.
 */

public class CountWord extends BaseRichBolt{

	
	private static final long serialVersionUID = -4206746564598093461L;
	private OutputCollector _outputCollector = null;
	private Map<String, MutableInt> _counter = null;
	
	public void prepare(@SuppressWarnings("rawtypes") Map stormConf, TopologyContext context,
			OutputCollector collector) {
		_outputCollector = collector;
		_counter = new HashMap<String, MutableInt>();
	}

	public void execute(Tuple input) {		
		String word = input.getString(0);
		MutableInt count = _counter.get(word);
		if (count == null) {
			count = new MutableInt();				
		}
		count.increment();
		_counter.put(word, count);
		_outputCollector.emit(new Values(word, count));
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("word", "count"));
	}
}
