/* Author : VivekVetri <vivekvetri@icloud.com> */

/*This file is part of Tweet-Trender.

    Tweet-Trender is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tweet-Trender is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tweet-Trender.  If not, see <http://www.gnu.org/licenses/>.
 */   	

package bolt;

import java.util.Map;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;

public class PrintCount extends BaseRichBolt {

	/**
	 * Bolt to print the current count
	 */
	private static final long serialVersionUID = -6133323709164892042L;

	public void prepare(@SuppressWarnings("rawtypes") Map stormConf, TopologyContext context,
			OutputCollector collector) {
		// do nothing

	}

	public void execute(Tuple tuple) {
		String word = tuple.getString(0);
		Object count = tuple.getValue(1);
		
		System.out.println(word +":" + count.toString());

	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// do nothing
	}

}
