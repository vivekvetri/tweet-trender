/* Author : VivekVetri <vivekvetri@icloud.com> */

/*This file is part of Tweet-Trender.

    Tweet-Trender is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tweet-Trender is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tweet-Trender.  If not, see <http://www.gnu.org/licenses/>.
 */   	

package bolt;

import java.util.Map;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

public class SplitTweet extends BaseRichBolt {

	/**
	 * Bolt to split the tweets into words
	 */
	private static final long serialVersionUID = 3046647784643054989L;
	private static final String REGEX_SPLIT = 
			"[,;\\s\":'<>()\\[\\]\\^\\!\\$\\%\\&\\*\\-" +
			"\\+=_\\{\\}\\.\\?\\\\/]+";
	
	private OutputCollector _outputCollector = null;
	
	public void prepare(@SuppressWarnings("rawtypes") Map stormConf, TopologyContext context,
			OutputCollector collector) {
		_outputCollector = collector;
		
	}

	public void execute(Tuple tuple) {
		Utils.sleep(100);
		String tweetStatus = tuple.getString(0);
		String[] words = tweetStatus.split(REGEX_SPLIT);	
		for(String word: words){
			_outputCollector.emit(new Values(word));
		}
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("word"));
	}

}
