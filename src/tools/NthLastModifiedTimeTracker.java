/* Author : VivekVetri <vivekvetri@icloud.com> */

/*This file is part of Tweet-Trender.

    Tweet-Trender is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Tweet-Trender is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Tweet-Trender.  If not, see <http://www.gnu.org/licenses/>.
 */   	

package tools;

import backtype.storm.utils.Time;
import org.apache.commons.collections.buffer.CircularFifoBuffer;

/**
 * This class tracks the time-since-last-modify of a "thing" in a rolling fashion.
 * <p/>
 * For example, create a 5-slot tracker to track the five most recent time-since-last-modify.
 * <p/>
 * You must manually "mark" that the "something" that you want to track -- in terms of modification times -- has just
 * been modified.
 */
public class NthLastModifiedTimeTracker {

  private static final int MILLIS_IN_SEC = 1000;

  private final CircularFifoBuffer lastModifiedTimesMillis;

  public NthLastModifiedTimeTracker(int numTimesToTrack) {
    if (numTimesToTrack < 1) {
      throw new IllegalArgumentException(
          "numTimesToTrack must be greater than zero (you requested " + numTimesToTrack + ")");
    }
    lastModifiedTimesMillis = new CircularFifoBuffer(numTimesToTrack);
    initLastModifiedTimesMillis();
  }

  private void initLastModifiedTimesMillis() {
    long nowCached = now();
    for (int i = 0; i < lastModifiedTimesMillis.maxSize(); i++) {
      lastModifiedTimesMillis.add(Long.valueOf(nowCached));
    }
  }

  private long now() {
    return Time.currentTimeMillis();
  }

  public int secondsSinceOldestModification() {
    long modifiedTimeMillis = ((Long) lastModifiedTimesMillis.get()).longValue();
    return (int) ((now() - modifiedTimeMillis) / MILLIS_IN_SEC);
  }

  public void markAsModified() {
    updateLastModifiedTime();
  }

  private void updateLastModifiedTime() {
    lastModifiedTimesMillis.add(now());
  }

}
